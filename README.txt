
#  PiCharMPD
#
#  Classes to interface with music player daemon and mpc using the Adafruit CharLCDPlate
#
#  LCDDisplay.py
#  
#  Based on tweetypi/hashtagdisplay 
#  Blog post: http://www.element14.com/community/groups/raspberry-pi/blog/2012/11/26/display-tweets-on-a-character-lcd
#
#  lcd_mpd_buttons.py
#
#  Interface with music player daemon and mpc using the Adafruit CharLCDPlate
#  Display cycles between current track, hostname and IP, and current date and time
#  Buttons control play/pause, next/prev track, software volume level
#
#  lcd_mpd_buttons2.py
#
#  This version uses mpd.py from python-mpd2 instead of command line calls to mpc
#  https://github.com/Mic92/python-mpd2/blob/master/mpd.py