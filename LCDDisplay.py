#!/usr/bin/env python
#
#  LCDDisplay.py
#  
#  Based on tweetypi/hashtagdisplay 
#  Blog post: http://www.element14.com/community/groups/raspberry-pi/blog/2012/11/26/display-tweets-on-a-character-lcd
#
#  TODO: 
#    * init param for whether to turn backlight on?
#    * convenience routines for polling the CharLCDPlate buttons (button combos pressed)?

from time import sleep
from Adafruit_I2C import Adafruit_I2C
from Adafruit_MCP230xx import Adafruit_MCP230XX
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate
import textwrap
import re
import sys
import smbus
import os

class LCDDisplay():
	def __init__(self,cols,rows,delay,debug=False):
		# number of columns on the character LCD (min: 16, max: 20)
		self.cols = cols
		# number of rows on the character LCD (min: 1, max: 4)
		self.rows = rows
		# duration in seconds to allow human to read LCD lines
		self.delay = delay
		# print messages to shell for debugging 
		self.debug = debug
		if debug == True:
			print " cols = {0}".format(cols)
			print " rows = {0}".format(rows)
			print "delay = {0}".format(delay)
			
		self.lcd = Adafruit_CharLCDPlate(busnum = 0)
		self.lcd.backlight(self.lcd.ON)
		self.lcd.begin(cols, rows)

	def printLines(self, lines):

		i = 0
		while i < lines.__len__():
			self.lcd.clear()
				
			# print line to each LCD row 
			for row in range(self.rows):

				# display line on current LCD row
				self.lcd.setCursor(0,row)
				self.lcd.message(lines[i])
				i=i+1
				# 200ms delay is now only for visual effect
				# initially added the delay to avoid issue 
				# where garbage characters were displayed:
				# https://github.com/adafruit/Adafruit-Raspberry-Pi-Python-Code/pull/13
				sleep(0.2)

				# no more lines remaining for this tweet
				if i >= lines.__len__():
					# sleep according to the number of rows displayed
					row_delay = self.delay / float(self.rows)
					delay = row_delay * (row+1)
					if(delay < 1):
						delay = 1
					sleep(delay)
					break

				# pause to allow human to read displayed rows
				if(row+1 >= self.rows):
					 sleep(self.delay)

	def display(self, msg):
		
			if self.debug == True:
				print msg
			# break lines into the width of LCD
			lines = textwrap.wrap(msg, self.cols)
			self.printLines(lines)


