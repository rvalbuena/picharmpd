#!/usr/bin/python

# 
# Interface with music player daemon and mpc using the Adafruit CharLCDPlate
# Display cycles between current track, hostname and IP, and current date and time
# Buttons control play/pause, next/prev track, software volume level

from LCDDisplay import LCDDisplay
from time import sleep, strftime
from datetime import datetime
import os


def readOutput(cmdline):
	#TODO: switch this to use process?
	f=os.popen(cmdline,'r',1)
	r = ""
	for i in f.readlines():
		r += i
	return r

def readHostName():
	# hostname and ip address
	# "hostname -I" works without having to check whether it's eth0 or wlan0 in use 
	return readOutput('hostname')+"\n"+readOutput('hostname -I')
	
def readDateTime():
	# current date and time
	return datetime.now().strftime("%a %x%n%I:%M %p %Z")

def readMpcCurrent():
	# current mpd track
	# TODO: use "mpc status" instead, would show error messages (i.e., if music on remote share is unavailable
	return readOutput("mpc current")


if __name__ == '__main__':
	# init with params specific to this display 16x2
	lcdDisplay = LCDDisplay(cols=16,rows=2,delay=1,debug=False)
	# read this once on start
	strHostName = readHostName()
	msgIndex = 0
	msg = ""

	try:
		while True:
			msg = ""
			
			# check for button presses and change the message right away
			# so the user sees the input was detected
			# TODO: check the readOutput string for error messages and display them?
			if (lcdDisplay.lcd.buttonPressed(lcdDisplay.lcd.SELECT)):
				# toggle play and pause
				msg = readOutput("mpc toggle | grep -i paused")
				lcdDisplay.display("[paused]" if (msg != "") else "[playing]" )
			elif (lcdDisplay.lcd.buttonPressed(lcdDisplay.lcd.LEFT)):
				# previous track in playlist, could also be "seek -10 seconds" (mpc seek -00:00:10)
				msg = readOutput("mpc prev")
				lcdDisplay.display("[previous]")
			elif (lcdDisplay.lcd.buttonPressed(lcdDisplay.lcd.RIGHT)):
				# next track in playlist, could also be "seek +10 seconds" (mpc seek +00:00:10)
				msg = readOutput("mpc next")
				lcdDisplay.display("[next]")
			# TODO: maybe use UP and DOWN to navigate other playlists and then SELECT 
			#   to start playing the newly selected one?
			elif (lcdDisplay.lcd.buttonPressed(lcdDisplay.lcd.UP)):
				# volume up
				msg = readOutput("mpc volume +10")
				lcdDisplay.display("[volume +10]")
			elif (lcdDisplay.lcd.buttonPressed(lcdDisplay.lcd.DOWN)):
				# volume down
				msg = readOutput("mpc volume -10")
				lcdDisplay.display("[volume -10]")
			else:
				# increment/wrap the message index
				# max 3 different messages right now, change upper limit as necessary
				msgIndex = 0 if (msgIndex >= 2) else msgIndex +1

				if (msgIndex == 0):
					# this was originally to keep any message from a button press from being
					# cleared too quickly 
					if (msg == ""):
						msg = readMpcCurrent()
					lcdDisplay.display(msg)
				# msgIndex == 1
				elif (msgIndex == 1):
					lcdDisplay.display(strHostName)
				# msgIndex == 2
				else:
					lcdDisplay.display(readDateTime())

			#sleep(1)
			# lcdDisplay.display() has small built-in delay
	except KeyboardInterrupt:
		# this ensures that the display clears and backlight turns off
		lcdDisplay.lcd.clear()
		lcdDisplay.lcd.backlight(lcd.OFF)
		
