#!/usr/bin/python

# 
# Interface with music player daemon and mpc using the Adafruit CharLCDPlate
# Display cycles between current track, hostname and IP, and current date and time
# Buttons control play/pause, next/prev track, software volume level
#
# This version uses mpd.py from python-mpd2 instead of command line calls to mpc
#
# This can be run on system startup by adding it to /etc/rc.local, i.e.:
#     python /<path to script>/lcd_mpd-buttons2.py
# 

from LCDDisplay import LCDDisplay
from time import sleep, strftime
from datetime import datetime
import os
import sys
from mpd import (MPDClient, CommandError)
from socket import error as SocketError

HOST = 'localhost'
PORT = '6600'
PASSWORD = False
CON_ID = { 'host':HOST, 'port':PORT }

def readOutput(cmdline):
	#TODO: switch this to use process?
	f=os.popen(cmdline,'r',1)
	r = ""
	for i in f.readlines():
		r += i
	return r

def readHostName():
	# hostname and ip address
	# "hostname -I" works without having to check whether it's eth0 or wlan0 in use 
	return readOutput('hostname')+"\n"+readOutput('hostname -I')
	
def readDateTime():
	# current date and time
	return datetime.now().strftime("%a %x%n%I:%M %p %Z")

## mpd related functions
def mpdConnect(client, con_id):
    """
    Simple wrapper to connect MPD.
    """
    try:
        client.connect(**con_id)
    except SocketError:
        return False
    return True

def mpdAuth(client, secret):
    """
    Authenticate
    """
    try:
        client.password(secret)
    except CommandError:
        return False
    return True
##

def waitForMpd(client, con_id, delay=0.5, count=60):
    """
    Keep trying to connect to MPD and wait
    """
	for x in range(0,count):
		if (mpdConnect(client, con_id) == True):
			print "Connected to MPD server"
			return True
		print "Failed to connect to MPD server, retrying"
		sleep(delay)		
	print "Failed to connect to MPD server"
	return False


def formatCurrentSong(state,cs):
	# note if state == stop there will be no current song
	#print state
	#print cs
	result = "[%s]" % state
	if ("artist" in cs.keys()):
		result += " %s" % cs["artist"]
		if ("title" in cs.keys()):
			result += " - %s" % cs["title"]
	elif ("filename" in cs.keys()):
		result += " %s" % cs["filename"]
	return result

if __name__ == '__main__':
	# init with params specific to this display 16x2
	lcdDisplay = LCDDisplay(cols=16,rows=2,delay=1,debug=False)
	# read this once on start
	strHostName = readHostName()

	msgIndex = 0
	msg = ""

	client = MPDClient()
    # even when started from rc.local, sometimes mpd wasn't ready yet,
    # so let's wait and keep retrying for a short period
	if (waitForMpd(client, CON_ID) == False):
		sys.exit(1)

	try:
		while True:
			
			# check current status
			cs = client.status()
			#
			vol = int(cs['volume'])
			state = cs['state']
			# check for error messages
			msg = cs['error'] if ('error' in cs.keys()) else ''

			# check for button presses and change the message right away
			# so the user sees the input was detected
			if (lcdDisplay.lcd.buttonPressed(lcdDisplay.lcd.SELECT)):
				# toggle play and pause
				# this should work even if mpd is paused/stopped from another client
				if (state == 'play'):
					client.pause(1)
				else:
					client.play()
				#client.pause(1 if (state == 'play') else 0)
				lcdDisplay.display("[pause]" if (state == 'play') else "[play]" )
			elif (lcdDisplay.lcd.buttonPressed(lcdDisplay.lcd.LEFT)):
				# previous track in playlist, could also be "seek -10 seconds" (mpc seek -00:00:10)
				client.previous()
				client.play()
				lcdDisplay.display("[previous]")
			elif (lcdDisplay.lcd.buttonPressed(lcdDisplay.lcd.RIGHT)):
				# next track in playlist, could also be "seek +10 seconds" (mpc seek +00:00:10)
				client.next()
				client.play()
				lcdDisplay.display("[next]")
			# TODO: maybe use UP and DOWN to navigate other playlists and then SELECT 
			#   to start playing the newly selected one?
			elif (lcdDisplay.lcd.buttonPressed(lcdDisplay.lcd.UP)):
				# volume up
				# range check because volume >100 throws an exception
				vol+=(10 if (vol <= 90) else 0)
				client.setvol(vol)
				lcdDisplay.display("[volume %d]" % vol)
			elif (lcdDisplay.lcd.buttonPressed(lcdDisplay.lcd.DOWN)):
				# volume down
				# range check here too
				vol-=(10 if (vol >= 10) else 0)
				client.setvol(vol)
				lcdDisplay.display("[volume %d]" % vol)
			else:
				# increment/wrap the message index
				# max 3 different messages right now, change upper limit as necessary
				msgIndex = 0 if (msgIndex >= 2) else msgIndex +1

				if (msgIndex == 0):
					# prevent an error message from being cleared too quickly 
					if (msg == ""):
						ts = client.currentsong()
						# TODO: safety check here in case there's nothing to print
						#msg = '['+state+'] '+ts['artist']+' - '+ts['title']
						msg = formatCurrentSong(state,ts)
					lcdDisplay.display(msg)
				# msgIndex == 1
				elif (msgIndex == 1):
					lcdDisplay.display(strHostName)
				# msgIndex == 2
				else:
					lcdDisplay.display(readDateTime())

	finally:
		# this ensures that the display clears and backlight turns off
		lcdDisplay.lcd.clear()
		lcdDisplay.lcd.backlight(lcdDisplay.lcd.OFF)
		client.disconnect()
